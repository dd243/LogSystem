﻿using BusinessLayer.Interface;
using Common;
using Configuration;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiService.Core.ApiControllers
{
    /// <summary>
    /// 信息日志管理-debug
    /// </summary>
    public class DebugLogController : ManagerController
    {
        public IDebugLogManager IDebugLogManager { get; set; }

        /// <summary>
        /// 查询平台
        /// </summary>
        /// <param name="query">查询条件</param>
        /// <returns></returns>
        [HttpGet]
        public AjaxReturnInfo Query([FromUri] LogQuery query)
        {
            var datas = IDebugLogManager.QueryLogInfo(query);
            return new AjaxReturnInfo()
            {
                Data = new
                {
                    List = datas,
                    query.Pagination,
                }
            };
        }

        /// <summary>
        /// 按照Id批量删除
        /// </summary>
        /// <param name="ids">id列表</param>
        /// <returns></returns>
        [HttpDelete]
        public AjaxReturnInfo Delete(List<string> ids)
        {
            IDebugLogManager.DeleteLog(ids);
            return new AjaxReturnInfo();
        }
    }
}
