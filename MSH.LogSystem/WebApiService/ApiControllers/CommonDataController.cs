﻿using BusinessLayer.Interface;
using Common;
using Configuration;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiService.Core.ApiControllers
{
    /// <summary>
    /// 公共数据服务
    /// </summary>
    public class CommonDataController : ManagerController
    {
        public IPlatformManager IPlatformManager { get; set; }

        /// <summary>
        /// 获取所有日志级别
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public AjaxReturnInfo QueryLogLevels()
        {
            var data = EnumExtension.GetAttributeInfo<LogLevel, NoteAttribute>("Note")
                .ToKeyValueList(x => x.Key, x => x.Value);
            return new AjaxReturnInfo()
            {
                Data = data,
            };
        }

        /// <summary>
        /// 获取所有平台
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public AjaxReturnInfo QueryPlatforms()
        {
            var datas = IPlatformManager.QueryPlatform(new PlatformQuery()
            {
                Pagination = new Pagination() { IsPaging = false }
            })
            .ToKeyValueList(x => x.Id, x => x.Name);
            return new AjaxReturnInfo()
            {
                Data = datas,
            };
        }
    }
}
